Simple program to demonstrate atan2 in C using OpenGL.

# Building
## Dependencies
* OpenGL version 3.3+
* GLFW 3
* stb_image.h
* freetype 2

## Linux
Build using:
```
make
```

## Msys
Build using:
```
./msys-build.sh
```
