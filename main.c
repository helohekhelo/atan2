#include <errno.h>
#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stb_image.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#define SCR_WIDTH 800
#define SCR_HEIGHT 600

#define RIGHT_SIZE 10

#define PI 3.14159265359
#define TO_DEG (180 / PI)

struct linep_s {
	unsigned int id;
	int col;
} linep;

struct textp_s {
	unsigned int id;
	int col;
} textp;

struct quadp_s {
	unsigned int id;
	int col;
} quadp;

struct slicep_s {
	unsigned int id;
	int col;
	int min;
	int max;
} slicep;

struct mouse_s {
	float x, y;
} mouse;

#define ANGLE_BET_SEG 16
#define ANGLE_BET_RADIUS 10
struct angle_bet_s {
	unsigned int vao, vbo;
	float data[(ANGLE_BET_SEG + 1) << 1];
} angle_bet;

struct tex_conf {
	int fmt;
	int filter;
	int wraps;
	int wrapt;
};

struct glyph {
	int w, h;
	int bx, by;
	int advx;
	unsigned int tex;
};

#define FONT_BEG 32
#define FONT_END 126
#define FONT_CNT (FONT_END - FONT_BEG + 1)
// sdf font
struct font {
	struct glyph glyphs[FONT_CNT];
	int maxby;
};

unsigned int quad_vao, quad_vbo;
unsigned int line_vao, line_vbo;

struct font def_font;

int vpw, vph;
float ihalf_vpw, ihalf_vph;
float half_vpw, half_vph;

struct opt_s {
	int use_deg;
} opt;

char *read_file(const char *file)
{
	FILE *fp;
	char *buf;
	long len;
	
	fp = fopen(file, "rb");
	if (fp == NULL) {
		fprintf(stderr, "%s: %s\n", file, strerror(errno));
		return NULL;
	}
	
	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	buf = malloc(len + 1);
	if (buf == NULL) {
		fprintf(stderr, "%s: malloc fail\n", file);
		return NULL;
	}
	buf[len] = '\0';

	fread(buf, len, 1, fp);
	fclose(fp);
	return buf;
}

bool comp_shader(const char *file, int type, unsigned int *id)
{
	char *src;
	int ok;

	src = read_file(file);
	if (src == NULL)
		return false;

	*id = glCreateShader(type);
	glShaderSource(*id, 1, (const char **)&src, NULL);
	glCompileShader(*id);
	
	glGetShaderiv(*id, GL_COMPILE_STATUS, &ok);
	if (!ok) {
		int len;
		char *log;

		glGetShaderiv(*id, GL_INFO_LOG_LENGTH, &len);
		log = malloc(len);
		if (log == NULL) {
			fprintf(stderr, "%s: log malloc fail\n", file);
		} else {
			glGetShaderInfoLog(*id, len, NULL, log);
			fprintf(stderr, "%s: %s\n", file, log);
			free(log);
		}
	}

	free(src);
	return ok;
}

bool comp_prog(const char *vert, const char *frag, unsigned int *id)
{
	unsigned int vid, fid;
	int ok;

	comp_shader(vert, GL_VERTEX_SHADER, &vid);
	comp_shader(frag, GL_FRAGMENT_SHADER, &fid);

	*id = glCreateProgram();
	glAttachShader(*id, vid);
	glAttachShader(*id, fid);
	glLinkProgram(*id);

	glGetProgramiv(*id, GL_LINK_STATUS, &ok);	
	if (!ok) {
		int len;
		char *log;

		glGetProgramiv(*id, GL_INFO_LOG_LENGTH, &len);
		log = malloc(len);
		if (log == NULL) {
			fprintf(stderr, "%s: log malloc fail\n", vert);
		} else {
			glGetProgramInfoLog(*id, len, NULL, log);
			fprintf(stderr, "%s: %s\n", vert, log);
			free(log);
		}
	}

	return ok;
}

int prog_get_loc(unsigned int id, const char *name)
{
	int loc = glGetUniformLocation(id, name);
	if (loc == -1)
		fprintf(stderr, "prog: %d '%s' uniform not found\n", id, name);
	return loc;
}

void comp_linep(void)
{
	if (!comp_prog("vline.glsl", "fline.glsl", &linep.id))
		exit(1);
	linep.col = prog_get_loc(linep.id, "col");
}

void comp_textp(void)
{
	if (!comp_prog("vtext.glsl", "ftext.glsl", &textp.id))
		exit(1);
	textp.col = prog_get_loc(textp.id, "col");
}

void comp_quadp(void)
{
	if (!comp_prog("vquad.glsl", "fquad.glsl", &quadp.id))
		exit(1);
	quadp.col = prog_get_loc(quadp.id, "col");
}

void comp_slicep(void)
{
	if (!comp_prog("vslice.glsl", "fslice.glsl", &slicep.id))
		exit(1);
	slicep.col = prog_get_loc(slicep.id, "col");
	slicep.min = prog_get_loc(slicep.id, "min");
	slicep.max = prog_get_loc(slicep.id, "max");
}

unsigned int load_tex(unsigned char *data, int w, int h,
	int dfmt, struct tex_conf *conf)
{
	unsigned int tex;

	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, conf->filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, conf->filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, conf->wraps);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, conf->wrapt);
	glTexImage2D(GL_TEXTURE_2D,
			0,
			conf->fmt,
			w,
			h,
			0,
			dfmt,
			GL_UNSIGNED_BYTE,
			data);
	return tex;
}

void freetype_err(const char *msg, FT_Error err)
{
	fprintf(stderr, "%s: %s\n", msg, FT_Error_String(err));
	exit(1);
}

void load_font(struct font *font, const char *file)
{
	FT_Library lib;
	FT_Face face;
	FT_Error err;
	FT_GlyphSlot slot;
	struct glyph *glyph;
	struct tex_conf tex_conf;
	int ch;
	
	err = FT_Init_FreeType(&lib);
	if (err)
		freetype_err("font load", err);

	err = FT_New_Face(lib, file, 0, &face);
	if (err)
		freetype_err("font load", err);

	tex_conf.fmt = GL_RED;
	tex_conf.filter = GL_LINEAR;
	tex_conf.wraps = GL_REPEAT;
	tex_conf.wrapt = GL_REPEAT;

	FT_Set_Pixel_Sizes(face, 0, 48);
	glyph = font->glyphs;
	slot = face->glyph;
	font->maxby = 0;
	for (ch = FONT_BEG; ch <= FONT_END; ++ch) {
		err = FT_Load_Char(face, ch, FT_LOAD_DEFAULT);
		if (err) {
			freetype_err("font load", err);
			goto out;
		}
		
		err = FT_Render_Glyph(slot, FT_RENDER_MODE_SDF);
		if (err) {
			freetype_err("font load", err);
			goto out;
		}

		glyph->w = slot->bitmap.width;
		glyph->h = slot->bitmap.rows;
		glyph->bx = slot->bitmap_left;
		glyph->by = slot->bitmap_top;
		glyph->advx = slot->advance.x >> 6;
		glyph->tex = load_tex(slot->bitmap.buffer, glyph->w, glyph->h,
			GL_RED, &tex_conf);
		if (glyph->by > font->maxby)
			font->maxby = glyph->by;

	out:
		++glyph;
	}

	err = FT_Done_Face(face);
	if (err)
		freetype_err("font load", err);

	err = FT_Done_FreeType(lib);
	if (err)
		freetype_err("font load", err);
}

unsigned int gen_quad_mesh(unsigned int *vbo)
{
	unsigned int vao, ebo;
	const unsigned int index[] = {
		0, 1, 2,
		3, 1, 2,
	};
	const size_t stride = sizeof(float) * 4;

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, vbo);
	glGenBuffers(1, &ebo);

	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 16, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0); // pos
	glVertexAttribPointer(0, 2, GL_FLOAT, false, stride, (void *)0);
	glEnableVertexAttribArray(1); // tex
	glVertexAttribPointer(1, 2, GL_FLOAT, false,
			stride, (void *)(sizeof(float) * 2));
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index), index, GL_STATIC_DRAW);
	glBindVertexArray(0);

	return vao;
}

unsigned int gen_line_mesh(unsigned int *vbo)
{
	unsigned int vao;

	glGenVertexArrays(1, &vao);
	glGenBuffers(1, vbo);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, *vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, (void *)0);
	glBindVertexArray(0);

	return vao;
}

void gen_angle_bet_mesh(void)
{
	glGenVertexArrays(1, &angle_bet.vao);
	glGenBuffers(1, &angle_bet.vbo);
	glBindVertexArray(angle_bet.vao);
	glBindBuffer(GL_ARRAY_BUFFER, angle_bet.vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(angle_bet.data),
		NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, (void *)0);
	glBindVertexArray(0);
}

void transform_vert(float ox, float oy, float *x, float *y)
{
	*x = ox * ihalf_vpw - 1;
	*y = (vph - oy) * ihalf_vph - 1;
}

void gen_quad(float *quad, float x0, float y0, float x1, float y1)
{
	float xmin, ymin, xmax, ymax;
	transform_vert(x0, y0, &xmin, &ymin);
	transform_vert(x1, y1, &xmax, &ymax);
	
	quad[0] = xmax; // vert 0
	quad[1] = ymax;
	quad[2] = 1;
	quad[3] = 1;

	quad[4] = xmax; // vert 1
	quad[5] = ymin;
	quad[6] = 1;
	quad[7] = 0;

	quad[8] = xmin; // vert 2
	quad[9] = ymax;
	quad[10] = 0;
	quad[11] = 1;

	quad[12] = xmin; // vert 3
	quad[13] = ymin;
	quad[14] = 0;
	quad[15] = 0;
}

void gen_line(float *line, float x0, float y0, float x1, float y1)
{
	float xmin, ymin, xmax, ymax;
	transform_vert(x0, y0, &xmin, &ymin);
	transform_vert(x1, y1, &xmax, &ymax);

	line[0] = xmin; // vert 0
	line[1] = ymin;
	line[2] = xmax; // vert 1
	line[3] = ymax;
}

// line strip
void gen_circle(float *circle, float x, float y, float r,
	float rmin, float rmax, int cnt)
{
	float div, rad = rmin, xoff, yoff;
	float *it = circle;
	int i;

	div = (rmax - rmin) / cnt;
	for (i = 0; i < cnt; ++i) {
		xoff = cos(rad) * r + x;
		yoff = sin(rad) * r + y;
		transform_vert(xoff, yoff, it, it + 1);
		rad += div;
		it += 2;
	}
		
	xoff = cos(rad) * r + x;
	yoff = sin(rad) * r + y;
	transform_vert(xoff, yoff, it, it + 1);
}

void calc_vp(int width, int height)
{
	vpw = width;
	vph = height;
	half_vpw = width * 0.5f;
	half_vph = height * 0.5f;
	ihalf_vpw = 2.f / width;
	ihalf_vph = 2.f / height;
}

void fb_size_cb(GLFWwindow *window, int width, int height)
{
	(void)window;
	glViewport(0, 0, width, height);
	calc_vp(width, height);
}

void curpos_cb(GLFWwindow *window, double mx, double my)
{
	(void)window;
	mouse.x = mx;
	mouse.y = my;
}

GLFWwindow *create_window(int width, int height, const char *title)
{
	GLFWwindow *window;

	if (!glfwInit()) {
		fputs("glfw init fail\n", stderr);
		exit(1);
	}

	glfwDefaultWindowHints();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_VISIBLE, false);

	window = glfwCreateWindow(width, height, title, NULL, NULL);
	glfwMakeContextCurrent(window);
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		fputs("opengl init fail\n", stderr);
		exit(1);
	}

	glfwSetFramebufferSizeCallback(window, fb_size_cb);
	glfwSetCursorPosCallback(window, curpos_cb);
	glfwSwapInterval(1);
	return window;
}

void gl_err(const char *msg)
{
	int err;
	const char *const names[] = {
		"GL_INVALID_ENUM",
		"GL_INVALID_VALUE",
		"GL_INVALID_OPERATION",
		"GL_STACK_OVERFLOW",
		"GL_STACK_UNDERFLOW",
		"GL_OUT_OF_MEMORY",
		"GL_INVALID_FRAMEBUFFER_OPERATION",
		"GL_CONTEXT_LOST",
	};

	while ((err = glGetError()) != GL_NO_ERROR)
		fprintf(stderr, "gl: %s(%d): %s\n", names[err - GL_INVALID_ENUM],
			err, msg);
}

void draw_quad(float *quad_vert, float x0, float y0, float x1, float y1)
{
	gen_quad(quad_vert, x0, y0, x1, y1);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 16, quad_vert);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
}

void draw_line(float *line_vert, float x0, float y0, float x1, float y1)
{
	gen_line(line_vert, x0, y0, x1, y1);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 4, line_vert);
	glDrawArrays(GL_LINES, 0, 2);
}

void draw_triangle(float *line_vert)
{
	glUniform4f(linep.col, 1, 0, 0, 1); // adjacent leg
	draw_line(line_vert, half_vpw, half_vph, mouse.x, half_vph);
	glUniform4f(linep.col, 0, 1, 0, 1); // opposite leg
	draw_line(line_vert, mouse.x, half_vph, mouse.x, mouse.y);
	glUniform4f(linep.col, 1, 0, 1, 1); // hypotenuse
	draw_line(line_vert, half_vpw, half_vph, mouse.x, mouse.y);
}

void draw_right_angle(float *line_vert, float dx, float dy)
{
	float ox, oy;

	glUniform4f(linep.col, 1, 1, 0, 1);
	ox = dx < 0 ? mouse.x + RIGHT_SIZE : mouse.x - RIGHT_SIZE;
	oy = dy < 0 ? half_vph - RIGHT_SIZE : half_vph + RIGHT_SIZE; 
	draw_line(line_vert, ox, oy, mouse.x, oy);
	draw_line(line_vert, ox, oy, ox, half_vph);
}

void draw_angle_bet(float rotz)
{
	glUniform4f(linep.col, 1, 1, 0, 1);
	gen_circle(angle_bet.data, half_vpw, half_vph, ANGLE_BET_RADIUS,
		0, rotz, ANGLE_BET_SEG);
	glBindVertexArray(angle_bet.vao);
	glBindBuffer(GL_ARRAY_BUFFER, angle_bet.vbo);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(angle_bet.data), angle_bet.data);
	glDrawArrays(GL_LINE_STRIP, 0, ANGLE_BET_SEG + 1);
}

void draw_text(const char *text, float x, float y, float scale)
{
	struct glyph *glyph;
	float px, py, pw, ph;
	float data[16];

	for (; *text != '\0'; ++text) {
		glyph = &def_font.glyphs[*text - FONT_BEG];
		px = x + glyph->bx * scale;
		py = y - glyph->by * scale;
		pw = glyph->w * scale;
		ph = glyph->h * scale;
		gen_quad(data, px, py, px + pw, py + ph);
		glBindTexture(GL_TEXTURE_2D, glyph->tex);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(data), data);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);
		x += glyph->advx * scale;
	}
}

void text_get_size(const char *text, float scale, float *w, float *h)
{
	struct glyph *glyph;
	float x = 0;

	for (; *text != '\0'; ++text) {
		glyph = &def_font.glyphs[*text - FONT_BEG];
		x += glyph->advx;
	}

	*w = x * scale;
	*h = def_font.maxby * scale;
}

void normalize_vec(float x, float y, float *nx, float *ny)
{
	if (x == 0 && y == 0) {
		*nx = 0;
		*ny = 0;
	} else {
		float len = 1 / sqrt(x * x + y * y);
		*nx = x * len;
		*ny = y * len;
	}
}

void halfway_vec(float x0, float y0, float x1, float y1, float *hx, float *hy)
{
	normalize_vec(x0, y0, &x0, &y0);
	normalize_vec(x1, y1, &x1, &y1);
	normalize_vec(x0 + x1, y0 + y1, hx, hy);
	if (*hx == 0 && *hy == 0)
		*hy = 1;
}

void draw_diagram(float dx, float dy, float rotz)
{
	const float text_size = 0.5;
	char str[32];
	float w, h;
	float x, y;

	glBindVertexArray(quad_vao);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vbo);
	glUseProgram(textp.id);
	glUniform4f(textp.col, 1, 1, 1, 1);

	sprintf(str, "%d", (int)dx);
	text_get_size(str, text_size, &w, &h);
	y = dy > 0 ? half_vph : half_vph + h;
	draw_text(str, half_vpw + (dx - w) * 0.5f, y, text_size);

	sprintf(str, "%d", (int)-dy);
	text_get_size(str, text_size, &w, &h);
	x = dx > 0 ? mouse.x : mouse.x - w;
	draw_text(str, x, half_vph + (dy + h) * 0.5f, text_size);
	
	if (opt.use_deg)
		sprintf(str, "%.3fdeg", -rotz * TO_DEG);
	else
		sprintf(str, "%.3frad", -rotz);
	text_get_size(str, text_size, &w, &h);
	halfway_vec(dx, dy, 1, 0, &x, &y);
	x = half_vpw + x * ANGLE_BET_RADIUS - (w * 0.5f * fabs(y));
	y = half_vph + y * ANGLE_BET_RADIUS;
	if (dy >= 0)
		y += h;
	draw_text(str, x, y, text_size);
}

void render(void)
{
	float line_vert[4];
	float quad_vert[16];
	float dx, dy, adx, ady;
	float rotz;

	glUseProgram(quadp.id);
	glUniform4f(quadp.col, 0.5f, 0.5f, 0.5f, 1);
	glBindVertexArray(quad_vao);
	glBindBuffer(GL_ARRAY_BUFFER, quad_vbo);
	draw_quad(quad_vert, half_vpw - 5, half_vph - 5,
		half_vpw + 5, half_vph + 5);

	glUseProgram(linep.id);
	glBindVertexArray(line_vao);
	glBindBuffer(GL_ARRAY_BUFFER, line_vbo);
	draw_triangle(line_vert);

	dx = mouse.x - half_vpw;
	dy = mouse.y - half_vph;
	adx = fabs(dx);
	ady = fabs(dy);
	if (adx > RIGHT_SIZE && ady > RIGHT_SIZE)
		draw_right_angle(line_vert, dx, dy);

	if (dx < 0) {
		glUniform4f(linep.col, 0, 1, 1, 1);
		draw_line(line_vert, half_vpw, half_vph, vpw, half_vph);
	}

	rotz = atan2(dy, dx);
	draw_angle_bet(rotz);
	draw_diagram(dx, dy, rotz);
}

int main(void)
{
	GLFWwindow *window;

	window = create_window(SCR_WIDTH, SCR_HEIGHT, "atan2");
	comp_linep();
	comp_textp();
	comp_quadp();
	comp_slicep();
	quad_vao = gen_quad_mesh(&quad_vbo);
	line_vao = gen_line_mesh(&line_vbo);
	gen_angle_bet_mesh();
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	load_font(&def_font, "font/arimo/ArimoNerdFont-Regular.ttf");

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	calc_vp(SCR_WIDTH, SCR_HEIGHT);
	glClearColor(0, 0, 0, 1);
	glfwShowWindow(window);
	while (!glfwWindowShouldClose(window)) {
		glClear(GL_COLOR_BUFFER_BIT);
		render();
		gl_err("err");
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
}

/*
 * Copyright (c) 2024 helohekhelo
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
