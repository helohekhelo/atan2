exec := atan2

build := build
srcd := .
src := $(shell find $(srcd) -name '*.c')
obj := $(src:$(srcd)/%.c=$(build)/%.o)

CC := gcc
CFLAGS := -std=c99 -Wall -Wextra -Wpedantic -pipe -O2 -g
CFLAGS-GLAD := -std=c99 -Wall -Wextra -pipe -O2 -g
CPPFLAGS := -Iinclude -I/usr/include/freetype2
LDFLAGS := -lglfw -lm -lfreetype

$(exec): $(obj)
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@

$(build)/glad.o: glad.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS-GLAD) $(CPPFLAGS) -c $^ -o $@

$(build)/%.o: %.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $^ -o $@

.PHONY: clean
clean:
	rm -rf $(build)
	rm -f $(exec)

# Copyright (c) 2024 helohekhelo
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
